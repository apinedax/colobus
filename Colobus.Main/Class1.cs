﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Colobus.Main
{
    public class NameUtils
    {
        public List<string> Names { get; set; } = new List<string>() {"Andres", "Jose", "Joan", "Felix" };

        public List<string> GetNamesWith(string letter)
        {
            return Names?.Where(a => a.StartsWith(letter)).ToList();
        }
    }
}
