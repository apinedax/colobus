﻿using System;

using Xamarin.Forms;

namespace Colobus
{
    public class AppDetailPage : MasterDetailPage
    {
        public NavigationPage Nav { get; set;}

        public AppDetailPage ()
        {
            Master = new MenuPage ();

            Nav = new NavigationPage ();

            Nav.PushAsync (new ListPage (), true);

            Detail = Nav;
        }
    }
}


