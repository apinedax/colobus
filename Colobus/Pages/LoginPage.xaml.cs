﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace Colobus
{
    public partial class LoginPage : ContentPage
    {
        public LoginViewModel ViewModel { get; set;}

        public LoginPage ()
        {
            var repository = DependencyService.Get<IUserRepository> ();
            ViewModel = new LoginViewModel (repository);

            InitializeComponent ();

            BindingContext = ViewModel;

            ViewModel.PropertyChanged += ViewModel_PropertyChanged;
        }

        void ViewModel_PropertyChanged (object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof (ViewModel.IsUserValid))
            {
                if (!ViewModel.IsUserValid)
                    DisplayAlert ("Error loging", "Usuario o password son incorrectos..", "Ok");
            }
        }
    }
}

