﻿using System;

using Xamarin.Forms;

namespace Colobus
{
    public class ListPage : ContentPage
    {
        public ListPage ()
        {
            var repository = DependencyService.Get<IUserRepository> ();

            var user = repository.GetUser (2);

            Content = new StackLayout
            {
                BackgroundColor = Color.Pink,
                Children = {
                    new Label { Text = string.Format("Hello {0} From List Page", user?.FullName) }
                }
            };
        }
    }
}


