﻿using System;

using Xamarin.Forms;

namespace Colobus
{
    public class MenuPage : ContentPage
    {

        public MenuPage ()
        {

            Title = "Master Page";

            Content = new StackLayout
            {
                BackgroundColor = Color.Blue
            };
        }


        private View CreateContent ()
        {

            var background = new Image
            {
                Aspect = Aspect.AspectFill
            };

            var loggedInUser = new Label
            {
                TextColor = Color.White,
                FontSize = 18,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };

            var userLayout = new StackLayout
            {
                Children =
                {
                    loggedInUser
                }
            };

            var layout = new RelativeLayout
            {
                VerticalOptions = LayoutOptions.Start,
                Children =
                {
                    {
                        background,
                        Constraint.RelativeToParent(p=>0),
                        Constraint.RelativeToParent(p=>0),
                        Constraint.RelativeToParent(p=>p.Width),
                        Constraint.RelativeToParent(p=>264)
                    },
                    {
                        new BoxView { Color = Color.Black, Opacity = 0.6 },
                        Constraint.RelativeToParent(p=>0),
                        Constraint.RelativeToParent(p=>0),
                        Constraint.RelativeToParent(p=>p.Width),
                        Constraint.RelativeToParent(p=>264)
                    },
                    {
                        userLayout,
                        Constraint.RelativeToParent(p=>0),
                        Constraint.RelativeToParent(p=>0),
                        Constraint.RelativeToParent(p=>p.Width),
                        Constraint.RelativeToParent(p=>264)
                    }
                }
            };

            var listView = new ListView
            {
                ItemTemplate = new DataTemplate (typeof (MenuViewCell)),
                SeparatorColor = Color.Transparent,
                SeparatorVisibility = SeparatorVisibility.None,
                RowHeight = 50,
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            return new StackLayout
            {
                Spacing = 0,
                Children =
                {
                    layout,
                    listView
                }
            };
        }
    }
}


