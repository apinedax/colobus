﻿using System;
namespace Colobus
{
    public interface IUserRepository
    {
        bool IsUserValid (string username, string password);

        User GetUser (int userId);

        bool CreateNewUser (User user);
    }
}

