﻿
using System;
using Colobus;
using Xamarin.Forms;

[assembly: Dependency (typeof (UserRepository))]
namespace Colobus
{
    public class UserRepository : IUserRepository
    {
        UserDatabaseAccess db;

        public UserRepository ()
        {
            db = new UserDatabaseAccess ();
        }

        public User GetUser (int userId)
        {
            return db.GetUserById (userId);
        }

        public bool CreateNewUser (User user)
        {
            var saved =  db.InsertUser (user);

            return saved > 0;
        }

        public bool IsUserValid (string username, string password)
        {
            return "jose".Equals (username);
        }
    }
}

