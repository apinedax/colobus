﻿using System;
using SQLite;

namespace Colobus
{
    public interface ISqLite
    {
        SQLiteConnection GetConnection ();
    }
}

