﻿using System;
using SQLite;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;

namespace Colobus
{
    public class UserDatabaseAccess
    {
        public SQLiteConnection DatabaseConnection { get; private set;}

        public UserDatabaseAccess ()
        {
            DatabaseConnection = DependencyService.Get<ISqLite> ().GetConnection();
            DatabaseConnection.CreateTable<User> ();
        }

        public int InsertUser (User user)
        {
            return DatabaseConnection.Insert (user);
        }

        public User GetUserById (int id)
        {
            return DatabaseConnection.Table<User> ().FirstOrDefault (a => a.Id == id);
        }

        public List<User> GetUsers ()
        {
            return DatabaseConnection.Table<User> ().ToList ();
        }
    }
}

