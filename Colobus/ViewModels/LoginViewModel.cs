﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Colobus
{
    public class LoginViewModel : INotifyPropertyChanged
    {
        readonly IUserRepository _userRepository;

        public event PropertyChangedEventHandler PropertyChanged;

        public string MainLabel
        {
            get;
            set;
        }

        public string Title { get; set; }

        public string UserNamePlaceholder { get; set; }

        public string UserName { get; set; }

        public string PasswordPlaceholder { get; set; }

        public string Password { get; set; }

        public string ForgotPasswordLabel { get; set; }

        public string LoginActionLabel { get; set; }

        public string Logo { get; set; }

        public string Header { get; set; }

        public Color LoginActionColor { get; set; }

        public ICommand ForgotPasswordCommand { get; set; }

        public ICommand LoginCommand { get; set; }

        bool _isValid;
        public bool IsUserValid 
        { 
            get { return _isValid;} 
            set 
            { 
                _isValid = value;
                RaisePropertyChanged (nameof(IsUserValid));
            }
        }

        public LoginViewModel (IUserRepository userRepository)
        {
            _userRepository = userRepository;

            Init ();
        }

        void Init ()
        {

            Title = "Sign in";

            UserNamePlaceholder = "Username or Email";

            PasswordPlaceholder = "Password";

            ForgotPasswordLabel = "Forgot your password?";

            LoginActionLabel = "Sign in";

            Logo = "logo";

            Header = "ColoBus";

            LoginActionColor = Color.FromHex ("3eb5e5");

            LoginCommand = new Command (OnLogin);


        }


        void OnLogin (object obj)
        {

            var isValid = _userRepository.IsUserValid (UserName, Password);


            if (!isValid)
            {
                IsUserValid = isValid;
                return;
            }

            Application.Current.MainPage = new AppDetailPage ();
                
        }

        void RaisePropertyChanged (string propertyName)
        {
            PropertyChanged?.Invoke (this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

