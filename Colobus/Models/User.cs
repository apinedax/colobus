﻿using System;
using SQLite;

namespace Colobus
{
    public class User
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set;}

        [NotNull]
        public string Name { get; set;}

        public string LastName { get; set; }

        [Column ("UserImage")]
        public string ImageName { get; set; }

        [MaxLength(50), Unique, NotNull]
        public string Email { get; set;}

        public DateTime CreatedOn { get; set; }

        public DateTime LastLogin { get; set; }

        public bool IsActive { get; set;}

        [Ignore]
        public string FullName => string.Format ("{0},{1}", LastName, Name);

        public User ()
        {
        }
    }
}

