﻿using Colobus.Main;
using Xamarin.Forms;

namespace Colobus
{
    public class NamePage : ContentPage
    {
        public NamePage ()
        {
            const string startText = "J";
            var util = new NameUtils ();

            var names = util.GetNamesWith (startText);

            var namesTogether = string.Join (",", names);

            var Header = new Label { TextColor = Color.Red, FontSize = 25.15, Text = "Hello ContentPage" };
            var users = new Label { TextColor = Color.Black, FontSize = 12.15, Text = string.Format ("Name Start with {0} {1}", startText, namesTogether) };

            var ejemploSack = new StackLayout
            {
                Padding = new Thickness (0, 20, 0, 0),
                Children = {
                    Header,

                    users
                }
            };

            var ejemploSack2 = new StackLayout
            {
                Padding = new Thickness (0, 20, 0, 0),
                Children = {
                    users
                }
            };

            Content = ejemploSack;
        }
    }
}


