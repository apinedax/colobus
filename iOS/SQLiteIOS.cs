﻿using System;
using System.IO;
using Colobus.iOS;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency (typeof (SQLiteIOS))]
namespace Colobus.iOS
{
    public class SQLiteIOS : ISqLite
    {
        public SQLiteIOS ()
        {
        }

        public SQLiteConnection GetConnection ()
        {
            var sqliteFilename = "colobus.db3";
            string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
            string libraryPath = Path.Combine (documentsPath, "..", "Library");
            var path = Path.Combine (libraryPath, sqliteFilename);
            // Create the connection
            var conn = new SQLiteConnection (path);
            // Return the database connection
            return conn;
        }
    }
}

