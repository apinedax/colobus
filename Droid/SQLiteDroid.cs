﻿using System;
using System.IO;
using Colobus.Droid;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(SQLiteDroid))]
namespace Colobus.Droid
{
    public class SQLiteDroid : ISqLite
    {
        public SQLiteDroid ()
        {
        }

        public SQLiteConnection GetConnection ()
        {
            var sqliteFilename = "colobus.db3";
            string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
            var path = Path.Combine (documentsPath, sqliteFilename);
            // Create the connection
            var conn = new SQLiteConnection (path);
            // Return the database connection
            return conn;
        }
    }
}

